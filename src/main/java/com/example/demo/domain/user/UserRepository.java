package com.example.demo.domain.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


//JpaRepository를 상속했으면 어노테이션이 없어도 Ioc등록이 자동으로된다.
public interface UserRepository extends JpaRepository<User,Integer> {

	// JPA query method
	@Modifying
	@Query(value="INSERT INTO User(username) VALUES (:username);",nativeQuery=true)
	void mSave(@Param("username") String username);

	User findByUsername(String username);

}
