package com.example.demo.web;

import com.example.demo.domain.user.User;
import com.example.demo.domain.user.UserRepository;
import com.example.demo.service.TestService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
public class ViewTestController {

    private final TestService testService;
  
    @GetMapping("/hello1")
    public String hello(Model model) {
       if(testService.findUserName("홍길동")==null) {
          testService.saveUserName("홍길동"); // "홍길동" INSERT   
       }       
        User user = testService.findUserName("홍길동"); // 

        model.addAttribute("user",user);

        return user.getUsername();
    }
}
